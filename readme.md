## Post Code Search

This appis build using the[laravel](https://laravel.com)framework witn[MongoDB](https://www.mongodb.com/). It has a console command to import post codes from[mysociety.org](http://parlvid.mysociety.org/os/). Once imported it has 2 actions. 
 - Search for a post code by partial string match
 - Find post codes near a lat and long
 
To start the environment run 
 
 `docker-compose up -d`
 
 Copy .env.example to .env
 
`docker-compose exec app composer install`

`docker-compose exec app supervisorctl restart all`

To import run
 
`docker-compose exec app php artisan base:import`
 
To search for postcode by partial match:

http://0.0.0.0:8080/api/postcode/search/{partial}

To search for postcode by latitude and longitude:

http://0.0.0.0:8080/api/postcode/nearby/{lat}/{long}
