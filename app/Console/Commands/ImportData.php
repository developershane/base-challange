<?php

namespace App\Console\Commands;

use App\Contracts\PostCodeRepository;
use App\Jobs\ImportBatch;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use ZipArchive;

class ImportData extends Command
{

    const DOWNLOAD_FILE_NAME = 'ONSPD_MAY_2018.zip';
    const DATA_FILE_PATH = 'Data' . DIRECTORY_SEPARATOR . 'multi_csv';
    const DATA_PATH = 'data';
    const DOWNLOAD_PATH = 'download';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:import';

    protected $postCodeRepository;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $client;

    /**
     * Create a new command instance.
     *
     * @param PostCodeRepository $postCodeRepository
     */
    public function __construct(PostCodeRepository $postCodeRepository)
    {
        parent::__construct();
        $this->postCodeRepository = $postCodeRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        if (!file_exists(storage_path(self::DOWNLOAD_PATH))) {
            mkdir(storage_path(self::DOWNLOAD_PATH));
        }
        if (!file_exists(storage_path(self::DOWNLOAD_PATH . DIRECTORY_SEPARATOR . self::DOWNLOAD_FILE_NAME))) {
            $this->downloadData();
            $this->deleteDirectory(self::DATA_PATH);
        }
        if (!file_exists(storage_path(self::DATA_PATH))) {
            $this->extractFile();
        }
        $this->importData();

    }

    public function downloadData()
    {
        $bar = null;
        $client = new Client();
        $timer = time();
        $this->info('Downloading ' . self::DOWNLOAD_FILE_NAME);
        $result = $client->request(
            'GET',
            'http://parlvid.mysociety.org/os/' . self::DOWNLOAD_FILE_NAME,
            [
                'progress' => function (
                    $downloadTotal,
                    $downloadedBytes,
                    $uploadTotal,
                    $uploadedBytes
                ) use (&$timer) {
                    if ($downloadTotal) {
                        $total = round($downloadedBytes / $downloadTotal * 100);
                        if ($downloadTotal == $downloadedBytes) {

                        } else {
                            if (time() - $timer > 5) {
                                $this->info($total . '% downloaded');
                                $timer = time();
                            }
                        }
                    }
                },
                'sink' => storage_path(self::DOWNLOAD_PATH . DIRECTORY_SEPARATOR . self::DOWNLOAD_FILE_NAME)
            ]
        );
        $this->info('Download complete');
    }

    public function extractFile()
    {
        if (!file_exists(storage_path(self::DATA_PATH))) {
            mkdir(storage_path(self::DATA_PATH));
        }
        $dataPath = storage_path(self::DATA_PATH);
        $this->deleteDirectory($dataPath);
        $timer = time();
        $zip = new ZipArchive;
        if ($zip->open(storage_path(self::DOWNLOAD_PATH . DIRECTORY_SEPARATOR . self::DOWNLOAD_FILE_NAME)) === TRUE) {
            $this->info('Extracting zip file');
            $numFiles = $zip->numFiles;
            $totalSize = $this->getTotalUncompressedSize($zip);
            $totalExtracted = 0;
            for ($i = 0; $i < $numFiles; $i++) {
                $stat = $zip->statIndex($i);

                $zip->extractTo($dataPath, $stat['name']);

                $totalExtracted += $stat['size'];


                if (time() - $timer > 5) {
                    $this->info(round($totalExtracted / $totalSize * 100) . '% extracted');
                    $timer = time();
                }
            }
            $zip->close();

        } else {
            $this->error('An error occurred');
        }
    }

    private function getTotalUncompressedSize(ZipArchive $zip)
    {

        $totalSize = 0;

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $fileStats = $zip->statIndex($i);
            $totalSize += $fileStats['size'];
        }

        return $totalSize;
    }

    private function deleteDirectory($dir)
    {
        if (!file_exists($dir)) return true;
        if (!is_dir($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) return false;
        }
        return rmdir($dir);
    }

    private function importData()
    {

        Redis::set('processed_count', 0);
        $this->postCodeRepository->deleteAll();
        $files = scandir(storage_path(self::DATA_PATH . DIRECTORY_SEPARATOR . self::DATA_FILE_PATH));
        foreach ($files as $item) {
            if ($item == '.' || $item == '..') continue;
            $this->info("Added {$item} for processing");
            ImportBatch::dispatch(storage_path(self::DATA_PATH . DIRECTORY_SEPARATOR . self::DATA_FILE_PATH) . DIRECTORY_SEPARATOR . $item);
        }
        $count = Redis::get('processed_count');
        $newCount = Redis::get('processed_count');
        while ($count < count($files) - 2) {
            sleep(1);
            $count = Redis::get('processed_count');
            if ($count != $newCount) {
                $this->info('Processed ' . $count . ' files');
                $newCount = $count;
            }
        }

        $this->postCodeRepository->addIndex('post_code_search');
        $this->postCodeRepository->addGeoSpatialIndex('location', '2dsphere');


    }
}
