<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 04/07/2018
 * Time: 22:45
 */

namespace App\Import;

use League\Csv\Reader;
use function snake_case;
use function strtolower;

class CsvReader extends Reader {

    protected function setHeader(int $offset): array {
        $header = parent::setHeader($offset);
        $header = array_filter($header, function($item){
            if($item){
                return true;
            }
            return false;
        });

        $header = array_map(function($item){
            return trim(strtolower(snake_case($item)));
        }, $header);

        return $header;
    }
}