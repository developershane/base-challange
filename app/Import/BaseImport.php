<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 04/07/2018
 * Time: 22:42
 */

namespace App\Import;


use League\Csv\Statement;

abstract class BaseImport implements \App\Contracts\BaseImport
{
    protected $columnMappings = [];
    private $currentRow;
    protected $rowIndex = 0;
    protected $totalRows = 0;

    public function handle($filePath)
    {
        ini_set('auto_detect_line_endings', TRUE);
        $reader = CsvReader::createFromPath($filePath);
        $reader->setHeaderOffset(0);
        $stmt = (new Statement());

        $this->totalRows = count($reader);
        $records = $stmt->process($reader);

        $this->preProcess();

        foreach ($records as $row) {
            $this->currentRow = $row;
            $this->rowIndex++;
            $haveValues = TRUE;
            foreach ($this->columnMappings as $key => $value) {
                if ($this->get($key) == '') {
                    $haveValues = FALSE;
                }
                break;
            }
            if ($haveValues) {
                $this->processRow();
            }
        }

        $this->postProcess();


        ini_set('auto_detect_line_endings', FALSE);
    }

    public abstract function preProcess();

    public abstract function processRow();

    public abstract function postProcess();

    protected function get($column)
    {
        $csvColumn = $this->columnMappings["{$column}"];
        return mb_convert_encoding(trim($this->currentRow["{$csvColumn}"]), 'UTF-8', 'UTF-8');
    }



}