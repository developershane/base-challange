<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 04/07/2018
 * Time: 22:56
 */

namespace App\Import;


use App\Contracts\PostCodeImport;
use App\Contracts\PostCodeRepository;

class ONSPDImport extends BaseImport implements PostCodeImport
{

    /**
     * @var PostCodeRepository
     */
    private $postCodeRepository;

    public function __construct(PostCodeRepository $postCodeRepository)
    {
        $this->postCodeRepository = $postCodeRepository;
    }

    protected $columnMappings = [
        'post_code' => 'pcd',
        'lat' => 'lat',
        'long' => 'long'
    ];

    private $batch = [];
    private $batchSize = 20000;
    private $timer;
    private $startTime;

    public function preProcess()
    {
        $this->timer = time();
        $this->startTime = time();
    }

    public function processRow()
    {

        $lat = (float)$this->get('lat');
        $long = (float)$this->get('long');
        $data = [
            'post_code' => $this->get('post_code'),
            'post_code_search' => str_replace(' ', '', $this->get('post_code')),
            'location' => [
                'type' => "Point",
                'coordinates' => [$long, $lat]
            ],
        ];
        if ($lat && $long) {
            $this->batch[] = $data;
            if (count($this->batch) == $this->batchSize) {
                $this->postCodeRepository->insert($this->batch);
                $this->batch = [];
            }
            if ($this->rowIndex == $this->totalRows) {
                $this->postCodeRepository->insert($this->batch);
            }
        }

    }

    public function postProcess()
    {

    }
}