<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 08/07/2018
 * Time: 22:29
 */

namespace App\Contracts;


interface BaseRepository
{
    public function deleteAll();
    public function insert($data);
    public function addIndex($name);
    public function addGeoSpatialIndex($name, $type);

}