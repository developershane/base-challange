<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 08/07/2018
 * Time: 22:29
 */

namespace App\Contracts;


interface PostCodeRepository extends BaseRepository
{

    public function findPostCodesByPartialMatch($partial);

    public function nearby($lat, $long);
}