<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 08/07/2018
 * Time: 22:38
 */

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class PostCodeRepository extends BaseRepository implements \App\Contracts\PostCodeRepository
{
    protected $collectionName = 'post_codes';

    public function findPostCodesByPartialMatch($partial)
    {
        return DB::collection($this->collectionName)->where('post_code_search', 'regexp', "/^{$partial}/i")->get();
    }

    public function nearby($lat, $long)
    {
        return DB::collection($this->collectionName)->where('location', 'near', [
            '$geometry' => [
                'type' => 'Point',
                'coordinates' => [
                    (float)$lat,
                    (float)$long,
                ],
            ],
            '$maxDistance' => 50,
        ])->get();
    }
}