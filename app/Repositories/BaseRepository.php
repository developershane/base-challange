<?php
/**
 * Created by PhpStorm.
 * User: shanedejager
 * Date: 08/07/2018
 * Time: 22:38
 */

namespace App\Repositories;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;

class BaseRepository implements \App\Contracts\BaseRepository
{
    protected $collectionName = '';
    public function deleteAll()
    {
        DB::collection($this->collectionName)->truncate();
    }

    public function addIndex($name)
    {
        Schema::table($this->collectionName, function (Blueprint $table) use($name) {
            $table->index($name);
        });
    }

    public function addGeoSpatialIndex($name, $type)
    {
        Schema::table($this->collectionName, function (Blueprint $table) use($name, $type) {
            $table->geospatial($name, $type);
        });
    }

    public function insert($data)
    {
        DB::collection($this->collectionName)->insert($data);
    }
}