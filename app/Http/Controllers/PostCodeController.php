<?php

namespace App\Http\Controllers;

use App\Contracts\PostCodeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostCodeController extends Controller
{

    /**
     * @var PostCodeRepository
     */
    private $postCodeRepository;

    public function __construct(PostCodeRepository $postCodeRepository)
    {
        $this->postCodeRepository = $postCodeRepository;
    }

    public function search($partial){
        $partial = str_replace(' ','',$partial);
        return $this->postCodeRepository->findPostCodesByPartialMatch($partial);
    }

    public function nearby($lat, $long){
        return $this->postCodeRepository->nearby((float)$lat, (float)$long);
    }
}
