<?php

namespace App\Providers;


use App\Contracts\PostCodeImport;
use App\Contracts\PostCodeRepository;
use App\Import\ONSPDImport;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function (JobProcessed $event) {
            Redis::set('processed_count', Redis::get('processed_count') + 1);
        });

        Queue::failing(function (JobFailed $event) {
            Redis::set('processed_count', Redis::get('processed_count') + 1);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostCodeRepository::class, function ($app) {
            return new \App\Repositories\PostCodeRepository();
        });

        $this->app->bind(PostCodeImport::class, function ($app) {
            return new ONSPDImport($app->make(PostCodeRepository::class));
        });
    }
}
