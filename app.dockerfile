FROM php:7.1-fpm

RUN apt-get update
RUN apt-get install -y autoconf pkg-config libssl-dev
RUN pecl install mongodb-1.2.2
RUN docker-php-ext-install bcmath
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini

# Install Laravel dependencies
RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        supervisor \
        && apt-get clean

RUN docker-php-ext-install iconv mcrypt mbstring \
    && docker-php-ext-install zip pdo_mysql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-install gd pcntl

RUN pecl install xdebug-2.6.0 \
    && docker-php-ext-enable xdebug

RUN mkdir -p /var/log/supervisor /usr/src/app
COPY supervisor.conf /etc/supervisor/supervisord.conf


CMD ["/usr/bin/supervisord"]

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer